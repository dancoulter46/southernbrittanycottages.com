<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\iCalEasyReader;
use App\Models\Admin\Property;
use App\Models\Admin\PropertyDate;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use View;

class AdminCalendarController extends Controller
{
    public function index(Request $request)
    {
        $property = Property::findOrFail($request->id);

        $name = $property->contentDefault()->first()->name;

        return View::make('admin.property.import', ['property' => $property, 'name' => $name]);
    }

    public function post(Request $request)
    {
        $file = $request->file('ical');
        $id = $request->id;
        $ical = new iCalEasyReader;
        $lines = $ical->load(file_get_contents($file));

        // Need to get existing dates for property then append on newly uploaded ones if they don't exist
        $property_dates = PropertyDate::where('property_id', $id)->first();
        if ($property_dates == null) {
            $property_dates = new PropertyDate();
            $property_dates->property_id = $id;
            $property_dates->save();
            $property_dates->fresh();
        }

        $dates = $property_dates->dates;

        foreach ($lines['VEVENT'] as $line) {
            $start_date = Carbon::parse($line['DTSTART']);
            $end_date = Carbon::parse($line['DTEND']);

            $period = CarbonPeriod::create($start_date, $end_date);

            foreach ($period as $date) {
                $dates[] = $date->format('m/d/Y');
            }
        }

        $data['dates'] = array_unique($dates);

        $property_dates->update($data);

        return redirect(route('admin_calendar_import', ['id' => $id, 'success' => 1]));
    }
}
