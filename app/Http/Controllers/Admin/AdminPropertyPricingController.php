<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminPropertyPricingController extends Controller
{
    public function index($id){
        $property = Property::findOrFail($id);
        $property->load('propertiesPrices');
        return view('admin.property.price', compact('property'));
    }
}
