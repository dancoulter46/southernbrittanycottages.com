<?php

namespace App\Http\Controllers;

use App\Models\Admin\Booking;
use App\Models\Admin\Owner;
use App\Models\Admin\Payment;
use App\Models\Admin\Property;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Omnipay\Omnipay;

class PaymentController extends Controller
{
    private $paypal_gw, $stripe_gw, $firstdata_gw, $worldpay_gw;

    /**
     * PaymentController constructor.
     */
    public function __construct()
    {
        $this->worldpay_gw = Omnipay::create('WorldPay_Json');
        $this->worldpay_gw->setServiceKey('T_S_d8933376-dcc1-447f-abcb-07dca270bbf9');
        $this->worldpay_gw->setMerchantId('f7812095-fcc9-45d1-a907-580776e0e928');
        $this->worldpay_gw->setClientKey('T_C_8291c52a-18cf-4511-bd91-19d882e51f67');
    }

    public function index(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required',
            'end_date' => 'required',
            'first_name' => 'required',
            'email' => 'required'
        ], [
            'start_date.required' => get_string('fill_all_fields'),
            'end_date.required' => get_string('fill_all_fields')
        ]);

        $default_language = default_language();
        $static_data = static_home();

        $gateways = [];
        if (get_setting('allow_paypal', 'payment')) {
            $gateways[0] = 'PayPal';
        }
        if (get_setting('allow_stripe', 'payment')) {
            $gateways[1] = 'Stripe';
        }
        if (get_setting('allow_firstdata', 'payment')) {
            $gateways[2] = 'FirstData';
        }
        if (get_setting('allow_firstdata', 'payment')) {
            $gateways[4] = 'WorldPay';
        }

//        $gateways[3] = 'BeContacted';
        $gateways[4] = 'WorldPay';

        // Get Dates
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $s_d = Carbon::createFromFormat('d/m/Y', $start_date);
        $e_d = Carbon::createFromFormat('d/m/Y', $end_date);

        $startDay = $s_d->copy()->format('l');
        $property = Property::findOrFail($request->property_id);

        if ($startDay != 'Saturday' && $property->start_night == 'sat') {
            return Redirect::back()->withErrors(['Property requires first day of booking to be a Saturday.']);
        } elseif ($startDay != 'Friday' && $property->start_night == 'fri') {
            return Redirect::back()->withErrors(['Property requires first day of booking to be a Friday.']);
        }

        // Calculate Total
        $days = ($s_d->diffInDays($e_d) > 0) ? $s_d->diffInDays($e_d) : 1;
        if ($days) {

            // Calculate Total and Fees
            $totalNoFormat = $property->getTotalPriceByDates($s_d->copy()->format('Y-m-d'), $e_d->copy()->subDay()->format('Y-m-d'));

            $fees = 0;
            $refundable = 0;
            $cleaningFee = 0;
            if (isset($property->fees['city_fee'])) {
                $refundable = $property->fees['city_fee'];
            }
            if (isset($property->fees['cleaning_fee'])) {
                $fees += $property->fees['cleaning_fee'];
            }

            // Convert To User's Currency
            $currency_code = get_setting('currency_code', 'site');
            $currency = currency()->getCurrency(Session::get('currency'));
            $currency = $currency['symbol'] ? $currency['symbol'] : '';
            $total = $totalNoFormat;
            if ($currency_code != Session::get('currency')) {
                $total = currency($totalNoFormat, $currency_code, Session::get('currency'), false);
                $fees = currency($cleaningFee, $currency_code, Session::get('currency'), false);
            }

            $grand_total = round(($totalNoFormat + $fees + $refundable) * 100) / 100;

            // If within 9 weeks of booking date, full amount must be paid.
            $plusNineWeeks = Carbon::now()->addWeeks(9);
            if ($s_d->lessThan($plusNineWeeks)) {
                $deposit_total = number_format($grand_total, 2);
            } else {
                $deposit_total = number_format(($totalNoFormat + $cleaningFee) * 0.25, 2); // Deposit of 25% required
            }

            // Guest Number and Property Name
            $owner_id = $property->user_id;
            $user_id = $static_data['user'] ? $static_data['user']->id : 0;
            $property_id = $property->id;
            $property = $property->content($default_language->id)->name;
            $guest_number = $request->guest_number;
            $email = $request->email;
            $phone = $request->phone;
            $first_name = $request->first_name;

            // Return View back
            return view('home.payment.index', compact('gateways', 'email', 'first_name', 'phone', 'owner_id', 'property_id', 'user_id', 'property', 'currency', 'static_data', 'guest_number', 'default_language', 'start_date', 'end_date', 'total', 'guest_number', 'fees', 'grand_total', 'deposit_total', 'refundable'));
        } else {
            return redirect()->back();
        }
    }


    public function payment(Request $request)
    {
        $default_language = default_language();
        $static_data = static_home();

        // Validate Request
        $this->validate($request, [
//            'gateway' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'terms' => 'required',
            'address' => 'required',
            'town' => 'required',
            'county' => 'required',
            'country' => 'required',
            'postcode' => 'required',
            'card_number' => 'required',
            'exp_month' => 'required',
            'exp_year' => 'required',
            'cvv' => 'required',
            'card_name' => 'required'
        ], [
//            'gateway.required' => get_string('choose_payment_method'),
            'start_date.required' => get_string('fill_all_fields'),
            'end_date.required' => get_string('fill_all_fields'),
            'terms.required' => 'You must accept our terms and conditions.'
        ]);

        $data['buy_points'] = get_string('pay_for_your_book') . ' - ' . $request->property_name;
        $data['description'] = get_string('booking') . ' - ' . $request->property_name;

        // Get Dates
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $request->gateway = 4;

        $s_d = Carbon::createFromFormat('d/m/Y', $start_date);
        $e_d = Carbon::createFromFormat('d/m/Y', $end_date);

        // Calculate Total
        $property = Property::findOrFail($request->property_id);
        $days = ($s_d->diffInDays($e_d) > 0) ? $s_d->diffInDays($e_d) : 1;
        if ($days) {
            // Calculate Total and Fees
            $totalNoFormat = $property->getTotalPriceByDates($s_d->copy()->format('Y-m-d'), $e_d->copy()->subDay()->format('Y-m-d'));
            $fees = 0;
            $refundable = 0;
            $cleaningFee = 0;
            if (isset($property->fees['city_fee'])) {
                $refundable = $property->fees['city_fee'];
            }
            if (isset($property->fees['cleaning_fee'])) {
                $cleaningFee = $property->fees['cleaning_fee'];
            }

            // Convert To User's Currency
            $currency_code = get_setting('currency_code', 'site');
            $currency = currency()->getCurrency(Session::get('currency'));
            $currency = $currency['symbol'] ? $currency['symbol'] : '';
            if ($currency_code != Session::get('currency')) {
                $total = currency($totalNoFormat, $currency_code, Session::get('currency'), false);
                $fees = currency($cleaningFee, $currency_code, Session::get('currency'), false);
            }

            $grand_total = round(($totalNoFormat + $cleaningFee + $refundable) * 100) / 100;

            // If within 9 weeks of booking date, full amount must be paid.
            $plusNineWeeks = Carbon::now()->addWeeks(9);
            if ($s_d->lessThan($plusNineWeeks)) {
                $deposit_total = number_format($grand_total, 2);
            } else {
                $deposit_total = number_format(($totalNoFormat + $cleaningFee) * 0.25, 2); // Deposit of 25% required
            }

        } else {
            return abort(404);
        }

//        if ($request->gateway == 0) {
//            $params = [
//                'cancelUrl' => route('book_payment_cancel'),
//                'returnUrl' => route('book_payment_success'),
//                'name' => $data['buy_points'],
//                'description' => $data['description'],
//                'amount' => $deposit_total,
//                'currency' => Session::get('currency'),
//                'user_id' => $request->user_id,
//                'owner_id' => $request->owner_id,
//                'property_id' => $request->property_id,
//                'guest_number' => $request->guest_number,
//                'start_date' => $request->start_date,
//                'end_date' => $request->end_date,
//                'email' => $request->email,
//                'phone' => $request->phone,
//                'first_name' => $request->first_name,
//                'gateway' => $request->gateway,
//                'property_name' => $request->property_name,
//            ];
//
//            Session::put('params', $params);
//            Session::save();
//
//            $gateway = $this->paypal_gw;
//            $response = $gateway->purchase($params)->send();
//            if ($response->isSuccessful()) {
//                return true;
//            } elseif ($response->isRedirect()) {
//                $response->redirect();
//            } else {
//                Session::flash('payment_status', ['status' => false, 'msg' => $response->getMessage()]);
//                return view('home.payment.status', compact('static_data', 'default_language'));
//            }
//        } elseif ($request->gateway == 1) {
//            $params = [
//                'name' => $data['buy_points'],
//                'description' => $data['description'],
//                'amount' => $deposit_total,
//                'currency' => Session::get('currency'),
//                'user_id' => $request->user_id,
//                'owner_id' => $request->owner_id,
//                'property_id' => $request->property_id,
//                'guest_number' => $request->guest_number,
//                'start_date' => $request->start_date,
//                'end_date' => $request->end_date,
//                'email' => $request->email,
//                'phone' => $request->phone,
//                'first_name' => $request->first_name,
//                'property_name' => $request->property_name,
//                'gateway' => $request->gateway,
//                'token' => $request->stripeToken,
//                'hear' => $request->hear,
//                'children' => $request->children,
//                'cot' => $request->cot,
//            ];
//            Session::put('params', $params);
//            Session::save();
//            $gateway = $this->stripe_gw;
//            $response = $gateway->purchase($params)->send();
//            if ($response->isSuccessful()) {
//                $stripeResponse = $response->getData();
//                if ($stripeResponse['paid'] && $stripeResponse['status'] === 'succeeded') {
//                    $user = Owner::where('user_id', $params['owner_id'])->first();
//                    if ($user) {
//                        $user->pending_balance += $params['amount'];
//                        $user->save();
//                    }
//
//                    $params['total'] = $params['amount'];
//                    $params['user_data']['first_name'] = $params['first_name'];
//                    $params['user_data']['email'] = $params['email'];
//                    $params['user_data']['phone'] = $params['phone'];
//                    $params['user_data']['hear'] = $params['hear'];
//                    $params['user_data']['cot'] = $params['cot'];
//                    $params['user_data']['children'] = $params['children'];
//                    $params['start_date'] = Carbon::createFromFormat('d/m/Y', $params['start_date'])->format('Y-m-d');
//                    $params['end_date'] = Carbon::createFromFormat('d/m/Y', $params['end_date'])->format('Y-m-d');
//                    $booking = Booking::create($params);
//                    $params['booking_id'] = $booking->id;
//                    $params['transaction'] = isset($stripeResponse['id']) ? $stripeResponse['id'] : '';
//                    $params['payment_method'] = 'Stripe';
//                    $params['host_commission'] = get_setting('host_commission', 'payment');
//                    Payment::create($params);
//
//                    // Send Mail
//                    $mail_data['guest_number'] = $params['guest_number'];
//                    $mail_data['start_date'] = $params['start_date'];
//                    $mail_data['total'] = $params['amount'];
//                    $mail_data['end_date'] = $params['end_date'];
//                    $mail_data['property'] = $params['property_name'];
//                    $mail_data['currency'] = Session::get('currency');
//                    $mail_data['subject'] = $static_data['strings']['booking'] . ' - ' . $static_data['site_settings']['site_name'];
//                    $mail_data['first_name'] = $params['first_name'];
//                    $mail_data['email'] = $params['email'];
//                    $mail_data['admin_email'] = $static_data['site_settings']['contact_email'];
//                    $mail_data['site_name'] = $static_data['site_settings']['site_name'];
//                    $mail_data['from'] = $static_data['strings']['from'];
//                    $mail_data['to'] = $static_data['strings']['to'];
//                    $mail_data['thank'] = $static_data['strings']['thank_you_for_book_mail'];
//                    $mail_data['regards'] = $static_data['strings']['regards'];
//                    $mail_data['str_property'] = $static_data['strings']['property'];
//                    $mail_data['str_guest_number'] = $static_data['strings']['guest_number'];
//                    $mail_data['str_total'] = $static_data['strings']['total'];
//                    $mail_data['mail_after_text_book'] = $static_data['strings']['mail_after_text_book'];
//
//                    // Create the mail and send it
//                    Mail::send('emails.booked', ['mail_data' => $mail_data], function ($m) use ($mail_data) {
//                        $m->from($mail_data['admin_email'], $mail_data['site_name']);
//                        $m->to($mail_data['email'], $mail_data['first_name'])->subject($mail_data['subject']);
//                    });
//
//                    if ($booking->owner) {
//                        $owner = $booking->owner;
//                        $mail_data['email'] = $owner->email;
//                        $mail_data['first_name'] = $owner->info->first_name;
//                        $mail_data['thank'] = $static_data['strings']['you_have_received_new_booking'];
//
//
//                        // Create the mail and send it
//                        Mail::send('emails.booked', ['mail_data' => $mail_data], function ($m) use ($mail_data) {
//                            $m->from($mail_data['admin_email'], $mail_data['site_name']);
//                            $m->to($mail_data['email'], $mail_data['first_name'])->subject($mail_data['subject']);
//                        });
//                    }
//
//                    Session::forget('params');
//                    return redirect()->route('book_payment_thank_you');
//                } else {
//                    Session::flash('payment_status', ['status' => false, 'msg' => $static_data['strings']['something_happened']]);
//                    return view('home.payment.status', compact('static_data', 'default_language'));
//                }
//            } elseif ($response->isRedirect()) {
//                $response->redirect();
//            } else {
//                Session::flash('payment_status', ['status' => false, 'msg' => $response->getMessage()]);
//                return view('home.payment.status', compact('static_data', 'default_language'));
//            }
//        } elseif ($request->gateway == 2) {
//            $params = [
//                'testMode' => 'true',
//                'amount' => '50',
//                'currency' => 'GBP',
//                'card' => [
//                    'number' => '4111111111111111',
//                    'expiryMonth' => '6',
//                    'expiryYear' => '2030',
//                    'cvv' => '123'
//                ],
//                'gateway' => $request->gateway,
//                'cancelUrl' => route('book_payment_cancel'),
//                'returnUrl' => route('book_payment_success'),
//            ];
//
//            Session::put('params', $params);
//            Session::save();
//
//            $gateway = $this->firstdata_gw;
//            $response = $gateway->purchase($params)->send();
//            if ($response->isSuccessful()) {
//                return true;
//            } elseif ($response->isRedirect()) {
//                $response->redirect();
//            } else {
//                Session::flash('payment_status', ['status' => false, 'msg' => $response->getMessage()]);
//                return view('home.payment.status', compact('static_data', 'default_language'));
//            }
//        } elseif ($request->gateway == 3) {
//            $params = [
//                'name' => $data['buy_points'],
//                'description' => $data['description'],
//                'amount' => $grand_total,
//                'currency' => Session::get('currency'),
//                'user_id' => $request->user_id,
//                'owner_id' => $request->owner_id,
//                'property_id' => $request->property_id,
//                'guest_number' => $request->guest_number,
//                'start_date' => $request->start_date,
//                'end_date' => $request->end_date,
//                'email' => $request->email,
//                'phone' => $request->phone,
//                'first_name' => $request->first_name,
//                'property_name' => $request->property_name,
//                'gateway' => $request->gateway,
//                'mobile' => $request->mobile,
//                'address' => $request->address,
//                'address2' => $request->address2,
//                'town' => $request->town,
//                'county' => $request->county,
//                'country' => $request->country,
//                'postcode' => $request->postcode,
//                'hear' => $request->hear,
//                'terms' => $request->terms,
//                'children' => $request->children,
//                'cot' => $request->cot,
//            ];
//            Session::put('params', $params);
//            Session::save();
//
//            $user = Owner::where('user_id', $params['owner_id'])->first();
//            if ($user) {
//                $user->pending_balance += $params['amount'];
//                $user->save();
//            }
//
//            $params['total'] = $params['amount'];
//            $params['user_data']['first_name'] = $params['first_name'];
//            $params['user_data']['email'] = $params['email'];
//            $params['user_data']['phone'] = $params['phone'];
//            $params['user_data']['mobile'] = $params['mobile'];
//            $params['user_data']['address'] = $params['address'];
//            $params['user_data']['address2'] = $params['address2'];
//            $params['user_data']['town'] = $params['town'];
//            $params['user_data']['county'] = $params['county'];
//            $params['user_data']['country'] = $params['country'];
//            $params['user_data']['postcode'] = $params['postcode'];
//            $params['user_data']['hear'] = $params['hear'];
//            $params['user_data']['terms'] = $params['terms'];
//            $params['user_data']['children'] = $params['children'];
//            $params['user_data']['cot'] = $params['cot'];
//
//            $params['start_date'] = Carbon::createFromFormat('d/m/Y', $params['start_date'])->format('Y-m-d');
//            $params['end_date'] = Carbon::createFromFormat('d/m/Y', $params['end_date'])->format('Y-m-d');
//            $booking = Booking::create($params);
//
//            return redirect(route('book_payment_success'));
//
//            Session::flash('payment_status', ['status' => false, 'msg' => 'Payment Failed']);
//            return view('home.payment.status', compact('static_data', 'default_language'));
//        } else

        if ($request->gateway == 4) {

            $params = [
                'name' => $data['buy_points'],
                'description' => $data['description'],
                'amount' => $grand_total,
                'deposit' => $deposit_total,
                'refundable' => $refundable,
                'currency' => Session::get('currency'),
                'user_id' => $request->user_id,
                'owner_id' => $request->owner_id,
                'property_id' => $request->property_id,
                'guest_number' => $request->guest_number,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'email' => $request->email,
                'phone' => $request->phone,
                'first_name' => $request->first_name,
                'property_name' => $request->property_name,
                'gateway' => $request->gateway,
                'mobile' => $request->mobile,
                'address' => $request->address,
                'address2' => $request->address2,
                'town' => $request->town,
                'county' => $request->county,
                'country' => $request->country,
                'postcode' => $request->postcode,
                'hear' => $request->hear,
                'cot' => $request->cot,
                'children' => $request->children,
                'terms' => $request->terms
            ];

            $billing_address = array(
                "address1" => $request->address,
                "address2" => $request->address2,
                "address3" => '',
                "postalCode" => $request->postcode,
                "city" => $request->town,
                "state" => $request->county,
                "countryCode" => 'GB',
            );

            $details = [
                'name' => $request->card_name,
                'card' => [
                    'number' => $request->card_number,
                    'expiryMonth' => $request->exp_month,
                    'expiryYear' => $request->exp_year,
                    'cvv' => $request->cvv,
                ]
            ];

            $payment = [
                'token' => $this->getToken($details),
                'testMode' => 'true',
                'amount' => $params['deposit'],
                'currency' => 'GBP',
                'name' => $request->card_name,
                'billingAddress' => $billing_address,
                'description' => $data['description'],
                'card' => [
                    'number' => $request->card_number,
                    'expiryMonth' => $request->exp_month,
                    'expiryYear' => $request->exp_year,
                    'cvv' => $request->cvv,
                ],
                'gateway' => $request->gateway,
                'cancelUrl' => route('book_payment_cancel'),
                'returnUrl' => route('book_payment_success'),
            ];

            $gateway = $this->worldpay_gw;
            $response = $gateway->purchase($payment)->send();

            if ($response->isSuccessful()) {
                $worldPayResponse = $response->getData();
                if ($worldPayResponse['orderCode'] && $worldPayResponse['paymentStatus'] === 'SUCCESS') {
                    $user = Owner::where('user_id', $params['owner_id'])->first();
                    if ($user) {
                        $user->pending_balance += $params['amount'];
                        $user->save();
                    }

                    $params['total'] = $params['amount'];
                    $params['user_data']['first_name'] = $params['first_name'];
                    $params['user_data']['email'] = $params['email'];
                    $params['user_data']['phone'] = $params['phone'];
                    $params['user_data']['mobile'] = $params['mobile'];
                    $params['user_data']['address'] = $params['address'];
                    $params['user_data']['address2'] = $params['address2'];
                    $params['user_data']['town'] = $params['town'];
                    $params['user_data']['county'] = $params['county'];
                    $params['user_data']['country'] = $params['country'];
                    $params['user_data']['postcode'] = $params['postcode'];
                    $params['user_data']['hear'] = $params['hear'];
                    $params['user_data']['cot'] = $params['cot'];
                    $params['user_data']['children'] = $params['children'];
                    $params['user_data']['terms'] = $params['terms'];

                    $params['start_date'] = Carbon::createFromFormat('d/m/Y', $params['start_date'])->format('Y-m-d');
                    $params['end_date'] = Carbon::createFromFormat('d/m/Y', $params['end_date'])->format('Y-m-d');
                    $booking = Booking::create($params);

                    $params['booking_id'] = $booking->id;
                    $params['transaction'] = isset($worldPayResponse['id']) ? $worldPayResponse['id'] : '';
                    $params['payment_method'] = 'Stripe';
                    $params['host_commission'] = get_setting('host_commission', 'payment');
                    Payment::create($params);

                    // Send Mail
                    $mail_data['guest_number'] = $params['guest_number'];

                    $mail_data['start_date'] = date('d/m/Y', strtotime($params['start_date']));
                    $mail_data['end_date'] = date('d/m/Y', strtotime($params['end_date']));

                    $mail_data['total'] = $params['amount'];
                    $mail_data['deposit'] = $params['deposit'];
                    $mail_data['refundable'] = $params['refundable'];
                    $mail_data['remaining'] = $params['amount'] - $params['deposit'];
                    $mail_data['property'] = $params['property_name'];
                    $mail_data['currency'] = Session::get('currency');
                    $mail_data['subject'] = $static_data['strings']['booking'] . ' - ' . $static_data['site_settings']['site_name'];
                    $mail_data['first_name'] = $params['first_name'];
                    $mail_data['phone'] = $params['phone'];
                    $mail_data['mobile'] = $params['mobile'];
                    $mail_data['address'] = $params['address'];
                    $mail_data['address2'] = $params['address2'];
                    $mail_data['town'] = $params['town'];
                    $mail_data['county'] = $params['county'];
                    $mail_data['country'] = $params['country'];
                    $mail_data['postcode'] = $params['postcode'];
                    $mail_data['email'] = $params['email'];
                    $mail_data['admin_email'] = $static_data['site_settings']['contact_email'];
                    $mail_data['site_name'] = $static_data['site_settings']['site_name'];
                    $mail_data['from'] = $static_data['strings']['from'];
                    $mail_data['to'] = $static_data['strings']['to'];
                    $mail_data['thank'] = "Thank you for booking! We will contact you with confirmation as soon as your deposit payment has been processed. Here are your booking details:";
                    $mail_data['regards'] = $static_data['strings']['regards'];
                    $mail_data['str_property'] = $static_data['strings']['property'];
                    $mail_data['str_guest_number'] = $static_data['strings']['guest_number'];
                    $mail_data['str_total'] = $static_data['strings']['total'];
                    $mail_data['mail_after_text_book'] = $static_data['strings']['mail_after_text_book'];

                    // Create the mail and send it
                    Mail::send('emails.booked', ['mail_data' => $mail_data], function ($m) use ($mail_data) {
                        $m->from($mail_data['admin_email'], $mail_data['site_name']);
                        $m->to($mail_data['email'], $mail_data['first_name'])->subject($mail_data['subject']);
                    });

                    if ($booking->owner) {
                        $owner = $booking->owner;
                        $mail_data['email'] = $owner->email;
                        $mail_data['first_name'] = $owner->info->first_name;
                        $mail_data['thank'] = $static_data['strings']['you_have_received_new_booking'];


                        // Create the mail and send it
                        Mail::send('emails.booked', ['mail_data' => $mail_data], function ($m) use ($mail_data) {
                            $m->from($mail_data['admin_email'], $mail_data['site_name']);
                            $m->to($mail_data['email'], $mail_data['first_name'])->subject($mail_data['subject']);
                        });
                    }

                    Session::forget('params');
                    return redirect()->route('book_payment_thank_you');
                } else {
                    Session::flash('payment_status', ['status' => false, 'msg' => $static_data['strings']['something_happened']]);
                    return view('home.payment.status', compact('static_data', 'default_language'));
                }
            } elseif ($response->isRedirect()) {
                Log::info($response);
                $response->redirect();
            } else {
                Session::flash('payment_status', ['status' => false, 'msg' => $response->getMessage()]);
                return view('home.payment.status', compact('static_data', 'default_language'));
            }
        }
        return 0;
    }

    public function paymentSuccess()
    {
        $default_language = default_language();
        $static_data = static_home();

        $params = Session::get('params');
        if ($params['gateway'] == 0) {
            $gateway = $this->paypal_gw;
            $response = $gateway->completePurchase($params)->send();
            $paypalResponse = $response->getData();
            if (isset($paypalResponse['ACK']) && $paypalResponse['ACK'] === 'Success') {
                $user = Owner::where('user_id', $params['owner_id'])->first();
                if ($user) {
                    $user->pending_balance += $params['amount'];
                    $user->save();
                }
                $params['total'] = $params['amount'];
                $params['user_data']['first_name'] = $params['first_name'];
                $params['user_data']['email'] = $params['email'];
                $params['user_data']['phone'] = $params['phone'];
                $params['start_date'] = Carbon::createFromFormat('d/m/Y', $params['start_date'])->format('Y-m-d');
                $params['end_date'] = Carbon::createFromFormat('d/m/Y', $params['end_date'])->format('Y-m-d');
                $booking = Booking::create($params);
                $params['booking_id'] = $booking->id;
                $params['transaction'] = isset($paypalResponse['PAYMENTINFO_0_TRANSACTIONID']) ? $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'] : '';
                $params['payment_method'] = 'PayPal';
                $params['host_commission'] = get_setting('host_commission', 'payment');
                Payment::create($params);

                // Send Mail
                $mail_data['guest_number'] = $params['guest_number'];
                $mail_data['start_date'] = $params['start_date'];
                $mail_data['total'] = $params['amount'];
                $mail_data['end_date'] = $params['end_date'];
                $mail_data['property'] = $params['property_name'];
                $mail_data['currency'] = Session::get('currency');
                $mail_data['subject'] = $static_data['strings']['booking'] . ' - ' . $static_data['site_settings']['site_name'];
                $mail_data['admin_email'] = $static_data['site_settings']['contact_email'];
                $mail_data['site_name'] = $static_data['site_settings']['site_name'];
                $mail_data['from'] = $static_data['strings']['from'];
                $mail_data['to'] = $static_data['strings']['to'];
                $mail_data['email'] = $params['email'];
                $mail_data['first_name'] = $params['first_name'];
                $mail_data['thank'] = $static_data['strings']['thank_you_for_book_mail'];
                $mail_data['regards'] = $static_data['strings']['regards'];
                $mail_data['str_property'] = $static_data['strings']['property'];
                $mail_data['str_guest_number'] = $static_data['strings']['guest_number'];
                $mail_data['str_total'] = $static_data['strings']['total'];
                $mail_data['mail_after_text_book'] = $static_data['strings']['mail_after_text_book'];

                // Create the mail and send it
                Mail::send('emails.booked', ['mail_data' => $mail_data], function ($m) use ($mail_data) {
                    $m->from($mail_data['admin_email'], $mail_data['site_name']);
                    $m->to($mail_data['email'], $mail_data['first_name'])->subject($mail_data['subject']);
                });

                if ($booking->owner) {
                    $owner = $booking->owner;
                    $mail_data['email'] = $owner->email;
                    $mail_data['first_name'] = $owner->info->first_name;
                    $mail_data['thank'] = $static_data['strings']['you_have_received_new_booking'];


                    // Create the mail and send it
                    Mail::send('emails.booked', ['mail_data' => $mail_data], function ($m) use ($mail_data) {
                        $m->from($mail_data['admin_email'], $mail_data['site_name']);
                        $m->to($mail_data['email'], $mail_data['first_name'])->subject($mail_data['subject']);
                    });
                }

                Session::forget('params');
                return redirect()->route('book_payment_thank_you');
            } else {
                Session::flash('payment_status', ['status' => false, 'msg' => $response->getMessage()]);
                return view('home.payment.status', compact('static_data', 'default_language'));
            }
        } elseif ($params['gateway'] == 3) {
            // Be contacted option
            Session::forget('params');
            return redirect()->route('book_payment_thank_you');
        }

        Session::flash('payment_status', ['status' => false, 'msg' => $static_data['strings']['something_happened']]);
        return view('home.payment.status', compact('static_data', 'default_language'));
    }

    public function paymentCancel()
    {
        $default_language = default_language();
        $static_data = static_home();

        Session::flash('payment_status', ['status' => false, 'msg' => $static_data['strings']['canceled_payment']]);
        return view('home.payment.status', compact('static_data', 'default_language'));
    }

    public function paymentThankYou()
    {
        $default_language = default_language();
        $static_data = static_home();

        Session::flash('payment_status', ['status' => true, 'msg' => $static_data['strings']['thank_you_pay_booking']]);
        return view('home.payment.status', compact('static_data', 'default_language'));
    }

    private function getToken($details = false)
    {
        $endpoint = 'https://api.worldpay.com/v1/tokens';

        $data = [
            'reusable' => false,
            'paymentMethod' => [
                "name" => $details['name'],
                "expiryMonth" => $details['card']['expiryMonth'],
                "expiryYear" => $details['card']['expiryYear'],
//                "issueNumber" => 1,
//                "startMonth" => 2,
//                "startYear" => 2013,
                "cardNumber" => $details['card']['number'],
                "type" => "Card",
                "cvc" => $details['card']['cvv'],
            ],
            'clientKey' => 'T_C_8291c52a-18cf-4511-bd91-19d882e51f67'
        ];

        $ch = curl_init();

        $data = json_encode($data);

        curl_setopt($ch, CURLOPT_URL, 'https://api.worldpay.com/v1/tokens');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        $result = json_decode($result);

        return !empty($result->token) ? $result->token : false;
    }
}
