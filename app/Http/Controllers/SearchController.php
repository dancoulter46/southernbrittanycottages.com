<?php

namespace App\Http\Controllers;

use App\Models\Admin\Booking;
use App\Models\Admin\Property;
use App\Models\Admin\PropertyContent;
use App\Models\Admin\PropertyDate;
use App\Models\Admin\Service;
use App\Models\Admin\ServiceContent;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $default_language, $static_data;

    public function __construct()
    {
        $this->default_language = default_language();
        $this->static_data = static_home();

    }

    public function index(Request $request)
    {
        $default_language = $this->default_language;
        $static_data = $this->static_data;

        $term = $request->keyword ? $request->keyword : '';

        // Handle Data
        $ids = [];
        if ($request->start_date != '' || $request->end_date != '') {
            // Bookings
            $start_date = ($request->start_date != '') ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::now()->format('Y-m-d');
            $end_date = ($request->end_date != '') ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::now()->addDay()->format('Y-m-d');
            $ids = Booking::whereDate('start_date', '<', $end_date)->WhereDate('end_date', '>', $start_date)->get()->pluck('property_id')->toArray();
            $dates = PropertyDate::whereNotIn('property_id', $ids)->whereNotNull('dates')->pluck('dates', 'property_id');

            // Owners booked dates
            $start_date = Carbon::createFromFormat('Y-m-d', $start_date);
            $end_date = Carbon::createFromFormat('Y-m-d', $end_date);
            if (!empty($dates)) {
                foreach ($dates as $key => $value) {
                    $id = $key;
                    foreach ($value as $value => $key) {
                        if (Carbon::createFromFormat('m/d/Y', trim($key))->between($start_date, $end_date)) {
                            $ids[] = $id;
                            break;
                        }
                    }
                }
            }
        }

        $property_ids = PropertyContent::where('name', 'LIKE', '%' . $term . '%')
            ->whereNotIn('property_id', $ids)
            ->get()->pluck('property_id');

        // If filtering by features is enabled
        $feature_ids = [];
        if (get_setting('filter_by_features', 'property')) {
            if ($request->features) {
                foreach ($request->features as $feature) {
                    $p = Property::where('features', 'NOT LIKE', '%"' . $feature . '"%')->pluck('id')->toArray();
                    array_push($feature_ids, $p);
                }
                if (count($feature_ids)) $feature_ids = array_unique(array_reduce($feature_ids, 'array_merge', []));
            }
        }

        $properties = Property::with(['images', 'contentload' => function ($query) use ($default_language) {
            $query->where('language_id', $default_language->id);
        }])->where('status', 1)->whereIn('id', $property_ids)->whereNotIn('id', $feature_ids);


        if ($request->location_id) {
            $properties->where('location_id', $request->location_id);
        }
        if ($request->category_id) {
            $properties->where('category_id', $request->category_id);
        }

        $properties = $properties->get();

        if (get_setting('services_allowed', 'service')) {

            $service_ids = ServiceContent::where('name', 'LIKE', '%' . $term . '%')->get()->pluck('service_id');

            $services = Service::with(['images', 'contentload' => function ($query) use ($default_language) {
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->whereIn('id', $service_ids);

            if ($request->location_id) {
                $services->where('location_id', $request->location_id);
            }
            if ($request->category_id) {
                $services->where('category_id', $request->category_id);
            }

            $services = $services->get();
        } else {
            $services = [];
        }

        if (get_setting('allow_featured_properties', 'property')) {
            $featured_properties = Property::with(['images', 'contentload' => function ($query) use ($default_language) {
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->where('featured', 1)->inRandomOrder()->take(6)->get();
        } else {
            $featured_properties = null;
        }

        if (get_setting('services_allowed', 'service') && get_setting('allow_featured_services', 'service')) {
            $featured_services = Service::with(['images', 'contentload' => function ($query) use ($default_language) {
                $query->where('language_id', $default_language->id);
            }])->where('status', 1)->where('featured', 1)->inRandomOrder()->take(6)->get();
        } else {
            $featured_services = null;
        }

        return view('home.search', compact('services', 'static_data', 'default_language', 'featured_properties', 'featured_services',
            'services', 'properties'));
    }
}
