<?php

namespace App\Models\Admin;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PropertyPricing extends Model
{
    protected $table = 'properties_prices';

    // Disable timestamps
    public $timestamps = false;

    // Allow columns to be filled with data
    protected $fillable = [
        'property_id',
        'start_date',
        'end_date',
        'price_per_night',
        'min_days'
    ];

    // Get the property the price belongs to
    public function property()
    {
        return $this->hasOne('App\Models\Admin\Property');
    }

    public function getStartDateUkAttribute()
    {
        return Carbon::createFromFormat('jS F Y', $this->start_date)->format('d/m/Y');
    }

    public function getEndDateUkAttribute()
    {
        return Carbon::createFromFormat('jS F Y', $this->end_date)->format('d/m/Y');
    }

    public function getStartDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('jS F Y');
    }

    public function getEndDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('jS F Y');
    }
}
