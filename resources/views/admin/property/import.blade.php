@extends('layouts.admin')

@section('title')
    <title>{{get_setting('site_name', 'site')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <div class="row">
        <div class="col l12 m12 s12 pull-right">
            <h3 class="page-title mbot10">iCal Import for {{ $name }}</h3>
            @if(!empty($_GET['success']))
                <div class="alert alert-success">
                    Dates uploaded successfully.
                </div>
            @endif
            {{ Form::open(array('url' => route('admin_calendar_import_post', $property->id), 'files'=>'true')) }}
            @csrf
            {{ 'Select the file to upload.' }}
            {{ Form::file('ical') }}
            {{ Form::submit('Upload File') }}
            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('footer')

@endsection