@extends('layouts.admin')

@section('title')
    <title>{{get_string('property_availability') . ' - ' . get_setting('site_name', 'site')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <div class="row">
        @section('page_title')
            <h3 class="page-title mbot10">{{get_string('property_availability')}}
                - {{ $property->contentDefault->name }}</h3>
        @endsection
        <div class="col l12 m12 s12 pull-right">
            <h3 class="page-title mbot10">Pricing</h3>
            <form method="post" action="{{ route('admin_property_update_pricing') }}">
            @csrf
            {{ Form::hidden('property_id', $property->id) }}
            <div class="col m6">
                <div class="form-group not-after">
                    <div class="input-group">
                        <span class="input-group-addon">From</span>
                        <input type="text" name="start_date" class="form-control datepicker filter-field"
                               placeholder="" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="col m6">
                <div class="form-group not-after">
                    <div class="input-group">
                        <span class="input-group-addon">To</span>
                        <input type="text" name="end_date"
                               class="form-control datepicker filter-field"
                               placeholder="" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="col m6">
                <div class="form-group not-after">
                    <div class="input-group">
                        <span class="input-group-addon">Price Per Night</span>
                        <input type="text" name="price_per_night"
                               class="form-control"
                               placeholder="" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="col m6">
                <div class="form-group not-after">
                    <div class="input-group">
                        <span class="input-group-addon">Min Nights</span>
                        <input type="text" name="min_days"
                               class="form-control"
                               placeholder="" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="col m12">
                <button class="btn waves-effect" type="submit" name="action">Add Pricing</button>
            </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col l12 m12 s12 pull-right">
            @if (!empty($property->propertiesPrices) && count($property->propertiesPrices) > 0)
                <div class="col m12">
                    <table class="table bordered striped">
                        <thead class="thead-inverse">
                        <tr>
                            <th></th>
                            <th>From date</th>
                            <th>To date</th>
                            <th>Price Per Night</th>
                            <th>Min Nights</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($property->propertiesPrices as $price)
                            <tr>
                                <td></td>
                                <td>{{ $price->start_date }}</td>
                                <td>{{ $price->end_date }}</td>
                                <td>{{ $price->price_per_night }}</td>
                                <td>{{ !empty($price->min_days) ? $price->min_days : 'No Min' }}</td>
                                <td><a class="edit-button" data-toggle="modal" data-target="#edit-modal" href="#"
                                       data-id="{{ $price->id }}"><i
                                                class="small material-icons color-green">edit</i></a> | <a
                                            class="delete-button" href="#" data-id="{{ $price->id }}"><i
                                                class="small material-icons color-red">delete</i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
        @endif
    </div>
@endsection

@section('footer')
    <div id="edit-modal" class="modal not-summernote fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#!" class="close" data-dismiss="modal" aria-label="Close"><i
                                class="material-icons">clear</i></a>
                    <strong class="modal-title">{{get_string('user_info')}}</strong>
                </div>
                <form method="post" action="{{ route('admin_property_update_pricing') }}">
                    @csrf
                    <div class="modal-body" id="price-details">
                        {{ Form::hidden('property_id', $property->id) }}
                        <div class="col m6">
                            <div class="form-group not-after">
                                <div class="input-group">
                                    <span class="input-group-addon">From</span>
                                    <input type="text" name="start_date" class="form-control datepicker filter-field"
                                           placeholder="" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col m6">
                            <div class="form-group not-after">
                                <div class="input-group">
                                    <span class="input-group-addon">To</span>
                                    <input type="text" name="end_date"
                                           class="form-control datepicker filter-field"
                                           placeholder="" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col m12">
                            <div class="form-group not-after">
                                <div class="input-group">
                                    <span class="input-group-addon">Price Per Night</span>
                                    <input type="text" name="price_per_night"
                                           class="form-control"
                                           placeholder="" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col m12">
                            <div class="form-group not-after">
                                <div class="input-group">
                                    <span class="input-group-addon">Min Nights</span>
                                    <input type="text" name="min_days"
                                           class="form-control"
                                           placeholder="" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="price_id" />
                    </div>
                    <div class="modal-footer">
                        <button class="btn waves-effect" type="submit" name="action">Update Pricing</button>
                        <a href="#!" class="waves-effect btn btn-default" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{URL::asset('assets/js/plugins/multidatespicker.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.edit-button').click(function (event) {
                event.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ url('/admin/property/getPricing/') }}',
                    type: 'post',
                    data: {price_id: id, _token: '{!! csrf_token() !!}'},
                    success: function (resp) {
                        $('#price-details input[name="start_date"]').val(resp.price.startDateUK);
                        $('#price-details input[name="end_date"]').val(resp.price.endDateUK);
                        $('#price-details input[name="price_id"]').val(resp.price.id);
                        $('#price-details input[name="price_per_night"]').val(resp.price.price_per_night);
                    },
                    error: function (resp) {
                        toastr.error(resp.responseJSON);
                    }
                });
            });

            $('.delete-button').click(function (event) {
                event.preventDefault();
                var id = $(this).data('id');
                var selector = $(this).parents('tr');
                bootbox.confirm({
                    title: '{{get_string('confirm_action')}}',
                    message: '{{get_string('delete_confirm')}}',
                    onEscape: true,
                    backdrop: true,
                    buttons: {
                        cancel: {
                            label: '{{get_string('no')}}',
                            className: 'btn waves-effect'
                        },
                        confirm: {
                            label: '{{get_string('yes')}}',
                            className: 'btn waves-effect'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                url: '{{ url('/admin/property/deletePricing/') }}',
                                type: 'post',
                                data: {id: id, _token: '{!! csrf_token() !!}'},
                                success: function (msg) {
                                    selector.remove();
                                    toastr.success(msg);
                                },
                                error: function (msg) {
                                    toastr.error(msg.responseJSON);
                                }
                            });
                        }
                    }
                });
            });

            // Generate datepicker
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy',
                minDate: 0,
            });
        });
    </script>
    <style>
        #ui-datepicker-div {
            z-index: 2000 !important;
        }
    </style>
@endsection