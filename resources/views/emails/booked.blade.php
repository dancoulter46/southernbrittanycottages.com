<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
        @import url('https://fonts.googleapis.com/css?family=Didact+Gothic');
        * {
            font-family: 'Didact Gothic', sans-serif !important;
        }
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>

<?php

$style = [
    /* Layout ------------------------------ */

        'body' => 'margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;',
        'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;',

    /* Masthead ----------------------- */

        'email-masthead' => 'padding: 25px 0; text-align: center;',
        'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;',

        'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
        'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
        'email-body_cell' => 'padding: 35px;',

        'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
        'email-footer_cell' => 'color: #AEAEAE; padding: 35px; text-align: center;',

    /* Body ------------------------------ */

        'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
        'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2; width: 100%;',

    /* Type ------------------------------ */

        'anchor' => 'color: #3869D4;',
        'header-1' => 'margin-top: 0; font-size: 19px; font-weight: bold; text-align: center;',
        'header-2' => 'margin-top: 0; font-size: 17px; font-weight: bold; text-align: center;',
        'paragraph' => 'margin-top: 0; font-size: 16px; line-height: 1.5em;',
        'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
        'paragraph-center' => 'text-align: center;',

    /* Buttons ------------------------------ */

        'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

        'button--green' => 'background-color: #22BC66;',
        'button--red' => 'background-color: #dc4d2f;',
        'button--blue' => 'background-color: #3869D4;',
];
?>

<body style="{{ $style['body'] }}">
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="{{ $style['email-wrapper'] }}" align="center">
            <table width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
                <tr>
                    <td style="{{ $style['email-masthead'] }}">
                        <a style="{{ $style['email-masthead_name'] }}" href="{{ url('/') }}" target="_blank">
                            <img src="{{ url('/assets/images/home/southern-brittany-cottages.jpg') }}" alt="Southern Brittany Cottages" />
                        </a>
                    </td>
                </tr>

                <!-- Email Body -->
                <tr>
                    <td style="{{ $style['email-body'] }}" width="100%">
                        <table style="{{ $style['email-body_inner'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding: 35px 35px 15px 35px; border-bottom: 1px solid #EDEFF2">
                                    <!-- Greeting -->
                                    <h1 style="{{ $style['header-1'] }}">
                                        {{ $mail_data['thank'] }}
                                    </h1>

                                    <!-- Intro -->
                                    <p style="{{ $style['paragraph'] }}">
                                        <strong>Property:</strong> {{ $mail_data['property'] }}<br/>
                                        <strong>From:</strong> {{ $mail_data['start_date'] }}<br/>
                                        <strong>To:</strong> {{ $mail_data['end_date'] }}<br/>
                                        <strong>Guests:</strong> {{ $mail_data['guest_number'] }}
                                    </p>
                                    <p style="{{ $style['paragraph'] }}">
                                        <strong>Refundable Security Deposit:</strong> &pound;{{ number_format($mail_data['refundable'], 2) }} <br/>
                                        <strong>Total:</strong> &pound;{{ number_format($mail_data['total'], 2) }} <br/>
                                        <strong>Amount Paid:</strong> &pound;{{ number_format($mail_data['deposit'], 2) }} <br/>
                                        <strong>Balance:</strong> &pound;{{ number_format($mail_data['remaining'], 2) }} <br/>
                                    </p>

                                    <h2 style="{{ $style['header-2'] }}">Your Details</h2>
                                    <p style="{{ $style['paragraph'] }}">
                                        <strong>Name:</strong> {{ !empty($mail_data['first_name']) ? $mail_data['first_name'] : '' }} <br/>
                                        <strong>Email:</strong> {{ !empty($mail_data['email']) ? $mail_data['email'] : '' }} <br/>
                                        <strong>Telephone:</strong> {{ !empty($mail_data['phone']) ? $mail_data['phone'] : '' }} <br/>
                                        <strong>Mobile:</strong> {{ !empty($mail_data['mobile']) ? $mail_data['mobile'] : '' }} <br/>
                                        <strong>Address Line 1:</strong> {{ !empty($mail_data['address']) ? $mail_data['address'] : '' }} <br/>
                                        <strong>Address Line 2:</strong> {{ !empty($mail_data['address2']) ? $mail_data['address2'] : '' }} <br/>
                                        <strong>Town:</strong> {{ !empty($mail_data['town']) ? $mail_data['town'] : '' }} <br/>
                                        <strong>County:</strong> {{ !empty($mail_data['county']) ? $mail_data['county'] : '' }} <br/>
                                        <strong>Country:</strong> {{ !empty($mail_data['country']) ? $mail_data['country'] : '' }} <br/>
                                        <strong>Postcode:</strong> {{ !empty($mail_data['postcode']) ? $mail_data['postcode'] : '' }} <br/>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 15px 35px 15px 35px; border-bottom: 1px solid #EDEFF2">
                                    <h3>Discounted Ferry Crossings & Travel Insurance</h3>
                                    <p>To take advantage of the discounts (20% on the cost of travel, not accommodation on board) with Brittany Ferries, click on this link (reading the instructions below first!)</p>

                                    <p><a href="https://www.brittany-ferries.co.uk/hfd-discount">www.brittany-ferries.co.uk</a></p>
                                    <p>Click on this link, and when you get to the part that asks for your special account number you need to insert C003P7 (this is your unique booking reference and 00 are zeros) and continue to confirm your booking information.</p>

                                    <p>Please note that the responsibility for your ferry booking will be between yourself and Brittany Ferries. If you have any problems booking on line please phone them on or phone on 03301597000.</p>

                                    <h3>Travel Insurance</h3>
                                    <p>Having researched the market place for you, I have found this company who offer the highest quality travel insurance for the most competitive rates; either one off or annual travel, and in most cases the children are covered FREE!  Click on this link to get your online quote - no obligation. </p>
                                    <p><a href="http://www.gotravelinsurance.co.uk/?advertid=53">www.gotravelinsurance.co.uk</a></p>

                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 15px 35px 35px 35px;">
                                    <p>In the meantime, if you have any questions, please don’t hesitate to ask.</p>

                                    <!-- Salutation -->
                                    <p style="{{ $style['paragraph'] }}">
                                        {{ $mail_data['regards'] }},<br>{{ $mail_data['site_name'] }}
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!-- Footer -->
                <tr>
                    <td>
                        <table style="{{ $style['email-footer'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style=" {{ $style['email-footer_cell'] }}">
                                    <p style="{{ $style['paragraph-sub'] }}">
                                        &copy; {{ date('Y') }}
                                        <a style="{{ $style['anchor'] }}" href="{{ url('/') }}" target="_blank">{{ $mail_data['site_name'] }}</a>.
                                        All rights reserved.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
