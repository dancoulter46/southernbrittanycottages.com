@extends('layouts.home_layout', ['static_data', $static_data])
@section('title')
    <title>404!</title>
@endsection

@section('content')
    <div class="row marginalized">
        <div class="col-sm-12">
            <h1 class="section-title-dark">404!</h1>
        </div>
    </div>
@endsection