<div class="container-fluid footer-container">
    <div class="container">
        <div class="row">
            {{--            <div class="col-md-3 col-sm-12 footer-widgets">--}}
            {{--                <h2 class="widget-title">{{ $static_data['strings']['about_us'] }}</h2>--}}
            {{--                <p>{{$static_data['strings']['opt_site_description']}}</p>--}}
            {{--            </div>--}}
            <div class="col-md-3 col-sm-12 footer-widgets">
                <h2 class="widget-title">{{ $static_data['strings']['opt_footer_menu1_head'] }}</h2>
                <ul class="footer-menu">
                    <li>
                        <a href="{{$static_data['design_settings']['footer_menu1_link1']}}">{{$static_data['strings']['opt_footer_menu1_text1']}}</a>
                    </li>
                    <li>
                        <a href="{{$static_data['design_settings']['footer_menu1_link2']}}">{{$static_data['strings']['opt_footer_menu1_text2']}}</a>
                    </li>
                    <li>
                        <a href="{{$static_data['design_settings']['footer_menu1_link3']}}">{{$static_data['strings']['opt_footer_menu1_text3']}}</a>
                    </li>
                    <li>
                        <a href="{{$static_data['design_settings']['footer_menu1_link4']}}">{{$static_data['strings']['opt_footer_menu1_text4']}}</a>
                    </li>
                    <li>
                        <a href="{{$static_data['design_settings']['footer_menu1_link5']}}">{{$static_data['strings']['opt_footer_menu1_text5']}}</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 footer-widgets">
            </div>
            <div class="col-md-5 col-sm-6 footer-widgets">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="widget-title">{{ $static_data['strings']['contact'] }}</h2>
                        <ul class="footer-menu">
                            @if($static_data['site_settings']['location_address'] || $static_data['site_settings']['location_city'] || $static_data['site_settings']['location_country'])
                                <li>
                                    <a href="#">{!! $static_data['site_settings']['location_address'].',<br> '.$static_data['site_settings']['location_city'].',<br/> '.$static_data['site_settings']['location_zip'].',<br/> '.$static_data['site_settings']['location_country'] !!}
                                    </a></li>@endif
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="footer-menu mt-4">
                            @if($static_data['site_settings']['contact_tel1'])
                                <li><a href="tel:{{ $static_data['site_settings']['contact_tel1'] }}"><i
                                                class="fa fa-phone"></i> {{ $static_data['site_settings']['contact_tel1'] }}
                                    </a>
                                </li>@endif
                            @if($static_data['site_settings']['contact_tel2'])
                                <li><a href="tel:{{ $static_data['site_settings']['contact_tel2'] }}"><i
                                                class="fa fa-phone"></i> {{ $static_data['site_settings']['contact_tel2'] }}
                                    </a>
                                </li>@endif
                            @if($static_data['site_settings']['contact_fax'])
                                <li><a href="tel:{{ $static_data['site_settings']['contact_fax'] }}"><i
                                                class="fa fa-fax"></i>{{ $static_data['site_settings']['contact_fax'] }}
                                    </a>
                                </li>@endif
                            @if($static_data['site_settings']['contact_email'])
                                <li><a href="mailto:{{ $static_data['site_settings']['contact_email'] }}"><i
                                                class="fa fa-envelope"></i>{{ $static_data['site_settings']['contact_email'] }}
                                    </a></li>@endif
                            {{--@if($static_data['site_settings']['contact_web'])
                                <li><a href="{{ $static_data['site_settings']['contact_web'] }}"><i
                                                class="fa fa-globe"></i> {{ $static_data['site_settings']['contact_web'] }}
                                    </a>
                                </li>@endif--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row copyright-row">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 copyright">
                    <p>{!! $static_data['strings']['copyright'] . date('Y') . ' ' . $static_data['strings']['rights_reserved'] . get_setting('site_name', 'site')!!}</p>
                </div>
                <div class="col-sm-6 powered-by">
                    <p>{!! $static_data['strings']['powered_by'] !!}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-message" style="z-index: 99999; position: fixed; bottom:0; right: 0; background: #a770a7; padding: 10px;">
        <a style="position: absolute; top:-2px; right: 2px;" href="#" onclick="TermsAndConditions()"><i class="fa fa-times" style="color: #fff"></i></a>
        <div class="inner" style="position: relative">
            <div class="circle" style="position: absolute; top:-35px; left:-35px; width: 50px; height: 50px; text-align: center; padding:12px; background: #a770a7; -webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px;">
                <i class="fa fa-info" style="color: #fff; font-size: 30px"></i>
            </div>
            <p style="color: #fff; margin: 0 !important; padding:0 12px">Need help browsing our site? <a href="/page/help-using-our-site" target="_blank">Click here</a></p>
        </div>
    </div>
</div>