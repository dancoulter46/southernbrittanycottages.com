<div class="col-sm-12">
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>{{ $static_data['strings']['nights'] }} </td>
                <td class="total-nights"><strong>{{ $total_nights }}</strong></td>
            </tr>
            @if(isset($cleaning_fee) && $cleaning_fee > 0)
                <tr>
                    <td>{{ $static_data['strings']['cleaning_fee'] }} </td>
                    <td class="cleaning-fee">
                        {{ userCurrencySymbol() }}
                        <strong>{{ currency($cleaning_fee, $static_data['site_settings']['currency_code'], Session::get('currency'),false) }}</strong>
                    </td>
                </tr>
            @endif
{{--            @if(isset($property->fees['city_fee']) && $property->fees['city_fee'] > 0)--}}
{{--                <tr>--}}
{{--                    <td>{{ $static_data['strings']['city_fee'] }} </td>--}}
{{--                    <td class="city-fee">--}}
{{--                        {{ userCurrencySymbol() }}--}}
{{--                        <strong>{{ currency($property->fees['city_fee'], $static_data['site_settings']['currency_code'], Session::get('currency'), false) }}</strong>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--            @endif--}}
            <tr>
                <td>{{ $static_data['strings']['total'] }} </td>
                <td class="total-book">{{ userCurrencySymbol() }} <strong>{{ $total_price }}</strong></td>
            </tr>
        </table>
    </div>
    @if(!get_setting('booking_by_payment', 'payment')) <a href="#"
                                                          class="primary-button book-now">{{ $static_data['strings']['book_now'] }}</a>
    @else
        <button type="submit"
                class="primary-button pay-now">{{ $static_data['strings']['book_now'] }}</button>
    @endif
    <p class="success-book green-color"
       style="display: none;">{{ $static_data['strings']['thank_you_for_book'] }}</p>
</div>
