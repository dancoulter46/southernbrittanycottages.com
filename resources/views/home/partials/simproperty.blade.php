<?php if (!isset($class)) $class = "col-md-4 col-sm-6 items-grid"; ?>
<div class="{{ $class }}">
    <div class="item box-shadow">
        <div id="carousel-{{$simproperty->id}}" class="main-image bg-overlay carousel slide" data-ride="carousel"
             data-interval="false">
            @if($simproperty->featured)
                <div class="featured-sign">
                    {{ $static_data['strings']['featured'] }}
                </div>
            @endif
            @if (isset($simproperty->propertiesPrices->first()->price_per_night))
                <div class="price">
                     <span class="currency">From</span> &pound;{{ currency((int)$simproperty->propertiesPrices->first()->price_per_night, $static_data['site_settings']['currency_code'], Session::get('currency'), false) }}
                    <span class="currency"> {{ $static_data['strings']['per_night'] }}</span>
                </div>
            @endif
            @if(count($simproperty->images))
                <div class="carousel-inner" role="listbox">
                    <?php $c = 0; ?>
                    @foreach($simproperty->images as $image)
                        <div class="carousel-item @if(!$c) active <?php $c++; ?> @endif">
                            <img class="responsive-img" src="{{ URL::asset('images/data').'/'.$image->image }}"/>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carousel-{{$simproperty->id}}" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">{{$static_data['strings']['previous']}}</span>
                </a>
                <a class="carousel-control-next" href="#carousel-{{$simproperty->id}}" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">{{$static_data['strings']['next']}}</span>
                </a>
            @else
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="responsive-img" src="{{ URL::asset('images/').'/no_image.jpg' }}"/>
                    </div>
                </div>
            @endif
        </div>
        <div class="data">
            <a href="{{url('/property').'/'.$simproperty->alias}}"><h3
                        class="item-title primary-color">{{ $simproperty->contentload->name }}</h3></a>
            <div class="item-category">{{$simproperty->location['address'].', '.$simproperty->location['city'] .' - '. $simproperty->location['country']}}</div>
            <div class="item-category">{{ $static_data['strings']['category'] .': '. $simproperty->category->contentload->name .' | ' }}
                {{ $static_data['strings']['location'] .': '. $simproperty->prop_location->contentload->location }}</div>
            <div class="item-category">{{ !empty($simproperty->property_info['size']) && $simproperty->property_info['size'] > 0 ? $static_data['strings']['size'] .': '. $simproperty->property_info['size'] . ' '. $static_data['site_settings']['measurement_unit']. ' | ' : ''}}
                {{ $static_data['strings']['rooms'] .': '. $simproperty->rooms .' | '}}
                {{ $static_data['strings']['guests'] .': '. $simproperty->guest_number}}</div>
{{--            @if($simproperty->user)--}}
{{--                <div class="small-text">{{ $static_data['strings']['posted_by'] .': '. $simproperty->user->username }}</div>@endif--}}
        </div>
    </div>
</div>