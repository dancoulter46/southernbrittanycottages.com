@extends('layouts.home_layout', ['static_data', $static_data])
@section('title')
    <title>{{$static_data['strings']['payments']}} - {{ $static_data['site_settings']['site_name'] }}</title>
    <meta charset="UTF-8">
    <meta name="title" content="{{ $static_data['site_settings']['site_name'] }}">
    <meta name="description" content="{{ $static_data['site_settings']['site_description'] }}">
    <meta name="keywords" content="{{ $static_data['site_settings']['site_keywords'] }}">
    <meta name="author" content="{{ $static_data['site_settings']['site_name'] }}">
    <meta property="og:title" content="{{ $static_data['site_settings']['site_name'] }}" />
    <meta property="og:image" content="{{URL::asset('/assets/images/home/').'/'.$static_data['design_settings']['slider_background']}}" />
@endsection
@section('bg')
    {{URL::asset('/assets/images/home/').'/'.$static_data['design_settings']['slider_background']}}
@endsection
@section('content')
    <div class="row  marginalized justify-content-center">
        <div class="col-sm-12">
            <h1 class="section-title-dark">{{$static_data['strings']['pay_for_your_book']}}</h1>
            @if (Session::has('activationSuccess'))
                <p class="section-subtitle-light text-centered green-color"><strong>{{ $static_data['strings']['account_successfully_activated'] }}</strong></p>
            @endif
        </div>
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <tr>
                        <th>{{ get_string('property') }}</th>
                        <th>{{ get_string('start_date') }}</th>
                        <th>{{ get_string('end_date') }}</th>
                        <th>{{ get_string('guest_number') }}</th>
                        <th>{{ get_string('total') }}</th>
                        <th>{{ get_string('fees') }}</th>
                        <th>Refundable Security Deposit</th>
                        <th>Total Rental</th>
                        <th>Due Today</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $property }}</td>
                            <td>{{ $start_date }}</td>
                            <td>{{ $end_date }}</td>
                            <td>{{ $guest_number }}</td>
                            <td>{{ $currency . $total }}</td>
                            <td>{{ $currency . $fees }}</td>
                            <td>{{ $currency . $refundable }}</td>
                            <td>{{ $currency . $grand_total }}</td>
                            <td>{{ $currency . $deposit_total }}*</td>
                        </tr>
                    </tbody>
                </table>
                <p>*The 25% deposit which is required before a booking can be confirmed is non-refundable.
                    You are advised to take out a Travel Insurance Policy with a Cancellation
                    Clause, which may enable you to recover non-refundable monies.</p>
            </div>
        </div>
        {!! Form::open(['method' => 'post', 'url' => route('booking_pay'), 'id' => 'payment-form', 'class' => 'col-sm-12']) !!}
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group mb-3">
                    <label for="first_name">Name</label>
                    <input type="text" class="form-control" id="first_name" placeholder="Joe Bloogs" name="first_name" required="required" value="{{ $first_name }}">
                </div>
                <div class="form-group mb-3">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" placeholder="joe@bloggs.com" name="email" required="required" value="{{ $email }}">
                </div>
                <div class="form-group mb-3">
                    <label for="phone">Phone Number</label>
                    <input type="text" class="form-control" id="phone" placeholder="" name="phone" required="" value="{{ $phone }}">
                </div>
                <div class="form-group mb-3">
                    <label for="mobile">Mobile</label>
                    <input type="text" class="form-control" id="mobile" placeholder="" name="mobile">
                </div>
                <div class="form-group mb-3">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" placeholder="1234 Main St" name="address" required="">
                </div>

                <div class="form-group mb-3">
                    <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                    <input type="text" class="form-control" id="address2" name="address2" placeholder="">
                </div>
                <div class="form-group mb-3">
                    <label for="town">Town</label>
                    <input type="text" class="form-control" id="town" name="town" placeholder="">
                </div>
                <div class="form-group mb-3">
                    <label for="county">County</label>
                    <input type="text" class="form-control" id="county" name="county" placeholder="">
                </div>
                <div class="row">
                    <div class="form-group col-md-8 mb-3">
                        <label for="country">Country</label>
                        <select class="custom-select d-block w-100" id="country" name="country" required="">
                            <option value="">Choose...</option>
                            <option>United Kingdom</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4 mb-3">
                        <label for="zip">Postcode</label>
                        <input type="text" name="postcode" class="form-control" id="postcode" placeholder="" required="">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group mb-3">
                    <label for="hear">Number of guests who are children (if applicable).</label>
                    <input type="text" name="children" class="form-control" id="children" placeholder="">
                    <p><small>Please include if they are boys or girls and their ages. We use this as some properties tailor bedding for kids.</small></p>
                </div>

                <div class="form-group mb-3">
                    <label for="hear">Do you require a cot or highchair?</label>
                    <select class="custom-select d-block w-100" id="cot" name="cot">
                        <option value="No">No</option>
                        <option value="Cot">Cot</option>
                        <option value="Highchair">Highchair</option>
                    </select>
                </div>

                <div class="form-group mb-3">
                    <label for="hear">How did you hear about us?</label>
                    <select class="custom-select d-block w-100" id="hear" name="hear" >
                        <option value="">Choose...</option>
                        <option value="Gites">Gites</option>
                        <option value="Holiday France Direct">Holiday France Direct</option>
                        <option value="Internet Search">Internet Search</option>
                        <option value="SimplyOwners">SimplyOwners</option>
                        <option value="Other">Other</option>
                    </select>
                </div>

                <div class="form-group mb-3">
                    <label for="card_number">Card Number</label>
                    <input type="text" class="form-control" id="card_number" name="card_number" placeholder="" required>
                </div>
                <div class="row">
                    <div class="form-group col-md-4 mb-3">
                        <label for="exp_month">Expiry Month</label>
                        <select class="custom-select d-block w-100" id="exp_month" name="exp_month" required="">
                            <option value="">Choose...</option>
                            @for ($i = 1; $i < 13; $i++)
                                <option value="{{$i}}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                            @endfor
                        </select>
                    </div>

                    <div class="form-group col-md-4 mb-3">
                        <label for="exp_year">Expiry Year</label>
                        <select class="custom-select d-block w-100" id="exp_year" name="exp_year" required="">
                            <option value="">Choose...</option>
                            @for ($i = date('y'); $i < date('y') + 7; $i++)
                            <option value="20{{$i}}">20{{$i}}</option>
                            @endfor
                        </select>
                    </div>

                    <div class="form-group col-md-4 mb-3">
                        <label for="cvv">CVV</label>
                        <input type="text" class="form-control" id="cvv" name="cvv" placeholder="" required="">
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label for="card_name">Name On Card</label>
                    <input type="text" class="form-control" id="card_name" name="card_name" placeholder="">
                </div>

                {{--                <div class="form-group  {{$errors->has('gateway') ? 'has-error' : ''}}">--}}
{{--                    <label>{{ $static_data['strings']['choose_payment_method'] }}</label>--}}
{{--                    {{Form::select('gateway', $gateways, null, ['class' => 'form-control', 'id' => 'type', 'required', 'placeholder' => $static_data['strings']['choose_payment_method']] )}}--}}
{{--                    @if($errors->has('gateway'))--}}
{{--                        <span class="wrong-error">* {{$errors->first('gateway')}}</span>--}}
{{--                    @endif--}}
{{--                </div>--}}
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" value="1" required class="" id="terms" name="terms" style="position:absolute; top:5px; left:0">
                    <label class="custom-control-label" for="terms">Please confirm you have read our terms and conditions. They are available <a href="/page/booking-terms-and-conditions" target="_blank">here</a>.</label>
                </div>
                <div class="stripe-payment hidden">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <p> {{ $static_data['strings']['enter_your_payment_details'] }}</p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div id="card-element"></div>
                            <div id="card-errors"></div>
                        </div>
                    </div>
                </div>
                {{ Form::hidden('user_id', $user_id) }}
                {{ Form::hidden('owner_id', $owner_id) }}
                {{ Form::hidden('property_id', $property_id) }}
                {{ Form::hidden('property_name', $property) }}
                {{ Form::hidden('start_date', $start_date) }}
                {{ Form::hidden('end_date', $end_date) }}
                {{ Form::hidden('guest_number', $guest_number) }}
                <button class="primary-button mtop20" type="submit" name="action">{{ $static_data['strings']['book_now'] }}</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('footer')
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#type").change(function(){
                var value = $(this).find("option:selected").val();
                var selector = $('.stripe-payment');
                switch (value){
                    case '1': selector.removeClass('hidden'); break;
                    default: selector.addClass('hidden'); break;
                }
            });
            var stripe = Stripe('{{ get_setting('stripe_public_api_key', 'payment') }}');
            var elements = stripe.elements();
            var style = {
                base: {
                    color: '#34495e',
                    lineHeight: '24px',
                    fontFamily: 'PT Sans',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#34495e'
                    }
                },
                invalid: {
                    color: '#D32F2F',
                    iconColor: '#D32F2F'
                }
            };
            var classes = {
                base: 'form-control',
            };
            var card = elements.create('card', {style: style, classes:classes});
            card.mount('#card-element');
            card.addEventListener('change', function(event) {
                const displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function(event) {
                event.preventDefault();
                if(!$('.stripe-payment').hasClass('hidden')){
                    stripe.createToken(card).then(function(result) {
                        if (result.error) {
                            var errorElement = document.getElementById('card-errors');
                            errorElement.textContent = result.error.message;
                        } else {
                            stripeTokenHandler(result.token);
                        }
                    });
                }else{
                    form.submit();
                }
            });
            function stripeTokenHandler(token) {
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);
                form.submit();
            }
        });
    </script>
@endsection